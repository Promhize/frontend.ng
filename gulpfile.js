// the boss
var gulp = require('gulp');



//postcss
var postcss = require('gulp-postcss');

//partials importer
var partialsimport = require('postcss-partial-import');

//autoprefixer
var autoprefixer = require('autoprefixer');

//alias
var postcssalias = require('postcss-alias');


//custom selectors
var customselector = require('postcss-custom-selectors');

//:matches() selector
var matchesselector = require('postcss-selector-matches');

//media variables
var mediaVariables = require('postcss-media-variables');

//custom media
var customMedia = require('postcss-custom-media');

//inlne media queries
var inlinemedia = require('postcss-inline-media');

//pack media queries
var packmedia = require('css-mqpacker');

//simple variables
var simpleVariables = require('postcss-simple-vars');

//postcss-mixins
var mixins =  require('postcss-mixins');

//responsive type
var responsiveType = require('postcss-responsive-type');

//rename
var rename = require("gulp-rename");


//rucksack
var rucksack = require('rucksack-css');








gulp.task('css', function () {
  var processors = [
    partialsimport,
  	postcssalias,
  	customselector,
 //  	colorfunction,
 //  	notselector,
 //  	matchesselector,
    // shopifyvarsyntax,
  	mediaVariables,
  	customMedia,
    inlinemedia,
    packmedia,
  	mixins,
  	simpleVariables,
  	mediaVariables,
    // calc,
  	responsiveType,
 //  	shortPosition,
 //  	bem,
 //  	nested,
 //  	simpleExtend,
  	autoprefixer,
    rucksack,
  ];
  return gulp.src('./css/raw/*.css')
    .pipe(postcss(processors))
    .pipe(rename("master.css"))
    .pipe(gulp.dest('./src/css'));
});




gulp.task('watch', function () {
   gulp.watch('./css/raw/*', ['css']);
   gulp.watch('./css/raw/partials/screens/agnostic/*', ['css']);
   gulp.watch('./css/raw/partials/screens/large/*', ['css']);
   // gulp.watch('css/raw/partials/*', ['css']);
   // gulp.watch('css/raw/partials/*', ['css']);
   // gulp.watch('css/raw/partials/*', ['css']);
   // gulp.watch('css/raw/partials/*', ['css']);
   // gulp.watch('css/raw/partials/*', ['css']);
});

gulp.task('start', ['browser-sync', 'watch']);
