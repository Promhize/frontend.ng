// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  'parser': 'sugarss',
  'from': '/css/master.css',
  'to': '/static/css/master.css',
  'plugins': {
    // to edit target browsers: use 'browserlist' field in package.json
    partialsimport: {},
  	postcssalias: {},
  	customselector: {},
  	mediaVariables: {},
  	customMedia: {},
    inlinemedia: {},
    packmedia: {},
  	mixins: {},
  	simpleVariables: {},
  	mediaVariables: {},
  	responsiveType: {},
  	autoprefixer: {},
    rucksack: {}
  }
}
